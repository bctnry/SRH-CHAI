OBJS=main.c
OBJ_NAME=srh-chai
CC=gcc

# change these according to your system setup.
SDL2_INCLUDE=C:\mingw-dev-lib\include
SDL2_LINK=C:\mingw-dev-lib\lib

# NOTE: i wanted to static link but failed to make it work, so a copy of SDL2 dynlib is
# required to run the generated executable.

# TERMINAL=-Wl,-subsystem,windows
TERMINAL=

asm: asm.c
	$(CC) asm.c -o $(OBJ_NAME)-asm

cli: main_cli.c
	$(CC) main_cli.c -o $(OBJ_NAME)-cli

main: main.c
	$(CC) main.c -I$(SDL2_INCLUDE) -L$(SDL2_LINK) -g $(TERMINAL) -lmingw32 -lSDL2main -lSDL2 -o $(OBJ_NAME)


all: $(OBJS)
	make cli
	make main
