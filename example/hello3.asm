
LDI SS, 0
LDI SP, 0xffff

JMP NEAR, MAIN

HELLO:
    LDI R0, 72
    OUT R0
    LDI R0, 101
    OUT R0
    LDI R0, 108
    OUT R0
    LDI R0, 108
    OUT R0
    LDI R0, 111
    OUT R0
    LDI R0, 32
    OUT R0
    LDI R0, 119
    OUT R0
    LDI R0, 111
    OUT R0
    LDI R0, 114
    OUT R0
    LDI R0, 108
    OUT R0
    LDI R0, 100
    OUT R0
    LDI R0, 10
    OUT R0
    RET

MAIN:
    LDI R1, 10
    LDI R2, 1
LOOPX:
    JZ NEAR, R1, EXIT
    CALL NEAR, HELLO
    SUB R1, R1, R2
    JMP NEAR, LOOPX
EXIT:
    HLT

