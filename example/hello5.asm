JMP NEAR, MAIN

HELLO:
    LDI R0, 72
    OUT R0
    LDI R0, 101
    OUT R0
    LDI R0, 108
    OUT R0
    LDI R0, 108
    OUT R0
    LDI R0, 111
    OUT R0
    LDI R0, 32
    OUT R0
    LDI R0, 119
    OUT R0
    LDI R0, 111
    OUT R0
    LDI R0, 114
    OUT R0
    LDI R0, 108
    OUT R0
    LDI R0, 100
    OUT R0
    LDI R0, 10
    OUT R0
    RET
    
TIMER_INTERRUPT:
    PUSH XH
    PUSH XL
    PUSH R0
    LDI XH, 0
    LDI XL, 0x201
    LDI R0, 0
    STM XH:XL, R0
    POP R0
    POP XL
    POP XH

    CALL NEAR, HELLO
    
    PUSH XH
    PUSH XL
    PUSH R0
    LDI XH, 0
    LDI XL, 0x20
    ; must be a number bigger than 4.
    LDI R0, 65535
    STM XH:XL, R0
    LDI XL, 0x21
    LDI R0, 1
    STM XH:XL, R0
    POP R0
    POP XL
    POP XH
    IRET

MAIN:
    LDA R0, R1, TIMER_INTERRUPT
    LDI XH, 0
    LDI XL, 0x100
    STM XH:XL, R0
    LDI XL, 0x101
    STM XH:XL, R1
    
    LDI XH, 0
    LDI XL, 0x20
    ; must be a number bigger than 4.
    LDI R0, 65535
    STM XH:XL, R0
    LDI XL, 0x21
    LDI R0, 1
    STM XH:XL, R0
LOOPX:
    JMP NEAR, LOOPX
    