#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

_Bool should_load_dump_file = false;

uint16_t* M;
uint16_t R[22];
#define XH R[16]
#define XL R[17]
#define YH R[18]
#define YL R[19]
#define SS R[20]
#define SP R[21]
size_t PC = 0;
size_t PC_BACKUP = 0;
_Bool should_exit;
_Bool should_debug;
char debug_cli_inputbuf[512];

void getkey(uint16_t* x) {
    int z = fgetc(stdin);
    *x = z == EOF? 0xffff : (z&0xff);
}

#define word32(H,L) ((((uint32_t)H)<<16)|L)

void chai_step(){
    uint16_t op = M[PC];
    uint16_t A, B, C;
    switch (op) {
        case 1: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            uint32_t target;
            switch (A) {
                case 0: target = (PC&0xffff0000)|R[B]; break;
                case 1: target = (PC&0xffff0000)|B; break;
                case 2: target = word32(R[B],R[C]); break;
                case 3: target = word32(B,C); break;
            }
            PC = target;
            return;
        }
        case 2: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            uint32_t target;
            switch (A>>8) {
                case 0: target = PC+R[B]; break;
                case 1: target = PC+B; break;
                case 2: target = PC+word32(R[A],R[B]); break;
                case 3: target = PC+word32(B,C); break;
            }
            PC = target;
            return;
        }
        case 3: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            uint16_t swtch = A>>8;
            uint32_t target;
            A = A&0xff;
            if (R[A] == 0) {
                switch (swtch) {
                    case 0: target = (PC&0xffff0000)|R[B]; break;
                    case 1: target = (PC&0xffff0000)|B; break;
                    case 2: target = word32(R[B],R[C]); break;
                    case 3: target = word32(B,C); break;
                }
                PC = target;
                return;
            }
            break;
        }
        case 4: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            uint16_t swtch = A>>8;
            uint32_t target;
            A = A&0xff;
            if (R[A] == 0) {
                switch (swtch) {
                    case 0: target = PC+R[B]; break;
                    case 1: target = PC+B; break;
                    case 2: target = PC+word32(R[B],R[C]); break;
                    case 3: target = PC+word32(B,C); break;
                }
                PC = target;
                return;
            }
            break;
        }
        case 5: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[B] = A==0? R[C] : C;
            break;
        }
        case 6: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = M[word32(R[B],R[C])];
            break;
        }
        case 7: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            M[word32(R[B],R[C])] = R[A];
            break;
        }
        case 8: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = (uint16_t)((((uint64_t)R[B]) + R[C]) % ((((uint64_t)1)<<32)));
            break;
        }
        case 9: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = (uint16_t)((((uint64_t)R[B]) + (((uint64_t)1)<<32) - R[C]) % ((((uint64_t)1)<<32)));
            break;
        }
        case 10: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = (uint16_t)((((uint64_t)R[B]) * R[C]) % ((((uint64_t)1)<<32)));
            break;
        }
        case 11: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = (uint32_t)((((uint64_t)R[B]) / R[C]) % ((((uint64_t)1)<<32)));
            break;
        }
        case 12: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = R[B] % R[C];
            break;
        }
        case 13: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = R[B] < R[C]? 1 : 0;
            break;
        }
        case 14: {
            A = M[PC+1];
            putchar(R[A]&0xff);
            break;
        }
        case 15: {
            A = M[PC+1];
            getkey(&R[A]);
            break;
        }
        case 16: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = R[B] & R[C];
            break;
        }
        case 17: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = R[B] | R[C];
            break;
        }
        case 18: {
            A = M[PC+1];
            B = M[PC+2];
            C = M[PC+3];
            R[A] = R[B] ^ R[C];
            break;
        }
        case 19: {
            A = M[PC+1];
            B = M[PC+2];
            R[A] = ~R[B];
            break;
        }
        case 20: {
            break;
        }
        case 21: {
            A = M[PC+1];
            M[word32(SS,SP)] = R[A];
            SP -= 1;
            break;
        }
        case 22: {
            A = M[PC+1];
            SP += 1;
            R[A] = M[word32(SS,SP)];
            break;
        }
        case 0: { break; }
        case 23: {
            should_exit = true;
            break;
        }
        case 24: {
            M[word32(SS,SP)] = (PC+8)>>16;
            M[word32(SS,SP)-1] = (PC+8)&0xffff;
            SP -= 2;
            break;
        }
        case 25: {
            PC = word32(M[word32(SS,SP)+2],M[word32(SS,SP)+1]);
            SP += 2;
            return;
        }
        case 27: {
            PC = PC_BACKUP;
            return;
        }
    }
    PC += 4;
}

_Bool load(char* path) {
    FILE* f;
    f = fopen(path, "r");

    if (!f) {
        fprintf(stderr, "Failed to open file.\n");
        return false;
    }
    int i = 0x1000;
    int ch;
    for (;;) {
        int ch1 = fgetc(f);
        if (feof(f)) { break; }
        int ch2 = fgetc(f);
        if (feof(f)) { break; }
        M[i] = (uint16_t)(ch1)|((uint16_t)(ch2)<<8);
        i++;
    }
    fclose(f);
    return true;
}

uint32_t brk;

// M[hex] - show memory content.
// B[hex] - one-time breakpoint.
// R - run till the end.
// S - step.
void debug_action() {
    char* subj = debug_cli_inputbuf;
    while (*subj && isspace(*subj)) { subj++; }
    switch (*subj) {
        case 0: {
            break;
        }
        case 'M': {

        }
    }
}

const char* USAGE = "usage: srh-chai-cli [-d|--debug] [-m memsize] [file]\n";

int main(int argc, char* argv[]) {
    should_exit = false;
    should_load_dump_file = false;
    int memsize;
    if (argc < 2) {
        fprintf(stderr, USAGE);
        // printf("usage: srh-chai-cli [-d] [-m memsize] [file]\n");
        return 1;
    }
    int argp = 1;
    // if (strcmp(argv[argp], "-d")) {
    //     should_load_dump_file = true;
    //     argp++;
    // }
    if (strcmp(argv[argp], "-d") == 0 || strcmp(argv[argp], "--debug") == 0) {
        argp++;
        should_debug = true;
    }
    if (strcmp(argv[argp], "-m") == 0) {
        argp++;
        memsize = atoi(argv[argp]) || 262140;
        argp++;
    } else {
        memsize = 262140;
    }
    if (argp >= argc) {
        fprintf(stderr, USAGE);
        // printf("usage: srh-chai-cli [-d] [-m memsize] [file]\n");
        return 1;
    }
    char* file = argv[argp];
    M = (uint16_t*)malloc(sizeof(uint16_t)*memsize);
    memset(R, 0, sizeof(R));
    memset(M, 0, sizeof(M));
    SS = 0x0;
    SP = 0xffff;
    PC = 0x1000;
    M[0x10] = (memsize-0x1000)&0xffff;
    M[0x11] = ((memsize-0x1000)>>16)&0xffff;
    if (!load(file)) {
        fprintf(stderr, "Failed to load.\n");
        return 1;
    }
    while (!should_exit) {
        chai_step();
        if (should_debug) {
            fprintf(stderr, "PC=%x XH=%x XL=%x YH=%x YL=%x SS=%x SP=%x\n", PC, XH, XL, YH, YL, SS, SP);
            fprintf(stderr, "R0=%x R1=%x R2=%x R3=%x R4=%x R5=%x R6=%x R7=%x\n", R[0], R[1], R[2], R[3], R[4], R[5], R[6], R[7]);
            fprintf(stderr, "R8=%x R9=%x R10=%x R11=%x R12=%x R13=%x R14=%x R15=%x\n", R[8], R[9], R[10], R[11], R[12], R[13], R[14]);
            fprintf(stderr, "Instruction: %x %x %x %x\n", M[PC], M[PC+1], M[PC+2], M[PC+3]);
            fprintf(stderr, ">> ");
            gets(debug_cli_inputbuf);
            debug_action();
        }
        if (M[0x21]) {
            M[0x20] -= 1;
            if (M[0x20] == 0) {
                PC_BACKUP = PC;
                PC = word32(M[0x101],M[0x100]);
            }
            if (should_debug) {
                fprintf(stderr, "Entering time interrupt, PC=%x PC_BACKUP=%x\n", PC, PC_BACKUP);
                fprintf(stderr, ">> ");
                gets(debug_cli_inputbuf);
                debug_action();
            }
        }
    }
    free(M);
    return 0;
}
