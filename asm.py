import re
import sys

REGEX_LABEL = re.compile(r'([a-zA-Z_][a-zA-Z0-9_]*):')
REGEX_REG = re.compile(r'(XH|XL|YH|YL|SS|SP)|R(\d+)')
REGEX_INSTR = re.compile(r'((JMP|JR|JZR)\s+(NEAR|FAR)\s+)')
REGEX_IMM = re.compile(r'(0|[1-9][0-9]*|0x[0-9a-f]+|0b[01]+)')
REGEX_CHAR = re.compile(r"'(.)'")
REGEX_ADDRESS = re.compile(r'(?:(XH|XL|YH|YL|SS|SP)|R(\d+)):(?:(XH|XL|YH|YL|SS|SP)|R(\d+))') 
REGEX_REPEAT = re.compile(r'(?:DW|DD)\s+(?:REPEAT\((.*)\)\s+)?(.*)')  

def main(path, generate_lst_file=False):
    with open(path, 'r') as f:
        s = f.read()

    label_dict = {}
    p = 0x1000
    s_lines = s.split('\n')
    for i, line in enumerate(s_lines):
        l = line.strip()
        if not l: continue
        if l.startswith(';'): continue
        g = REGEX_LABEL.match(l)
        if not g:
            if l.startswith('CALL') or l.startswith('LDA'):
                p += 8
            elif l.startswith('DW') or l.startswith('DD'):
                matchres = REGEX_REPEAT.match(l)
                if not matchres:
                    print(f'ERROR: ({i+1}) wrong format for DW/DD.')
                    continue
                times = matchres.groups()[0]
                times = 1 if not times or not times.strip() else int(times)
                pat = len(matchres.groups()[1].split(',')) * (2 if l.startswith('DD') else 1)
                p += times * pat
            else:
                p += 4
            continue
        if g.groups()[0] in label_dict:
            print(f'ERROR: ({i+1}) label "{g.groups()[0]}" defined at line {label_dict[g.groups()[0]]["line_num"]}', file=sys.stderr)
            continue
        label_dict[g.groups()[0]] = {
            'line_num': i+1,
            'p': p
        }

    def read_reg(x):
        matchres = REGEX_REG.fullmatch(x)
        if not matchres: return None
        return (
            16 if matchres.groups()[0] == 'XH'
            else 17 if matchres.groups()[0] == 'XL'
            else 18 if matchres.groups()[0] == 'YH'
            else 19 if matchres.groups()[0] == 'YL'
            else 20 if matchres.groups()[0] == 'SS'
            else 21 if matchres.groups()[0] == 'SP'
            else int(matchres.groups()[1])
        )
    def read_addr(x):
        matchres = REGEX_ADDRESS.fullmatch(x)
        if not matchres: return None
        a = (
            16 if matchres.groups()[0] == 'XH'
            else 17 if matchres.groups()[0] == 'XL'
            else 18 if matchres.groups()[0] == 'YH'
            else 19 if matchres.groups()[0] == 'YL'
            else 20 if matchres.groups()[0] == 'SS'
            else 21 if matchres.groups()[0] == 'SP'
            else int(matchres.groups()[1])
        )
        b = (
            16 if matchres.groups()[2] == 'XH'
            else 17 if matchres.groups()[2] == 'XL'
            else 18 if matchres.groups()[2] == 'YH'
            else 19 if matchres.groups()[2] == 'YL'
            else 20 if matchres.groups()[2] == 'SS'
            else 21 if matchres.groups()[2] == 'SP'
            else int(matchres.groups()[3])
        )
        return (a, b)
    
    def read_imm(x):
        matchres = REGEX_IMM.fullmatch(x)
        if not matchres:
            matchres = REGEX_CHAR.fullmatch(x)
            if not matchres: return None
            return ord(matchres.groups()[0])
        z = matchres.groups()[0]
        return (
            int(z[2:], 16) if z.startswith('0x')
            else int(z[2:], 2) if z.startswith('0b')
            else int(z, 10)
        )

    output = []
    def write_word(x):
        nonlocal output
        output.append(x&0xff)
        output.append((x>>8)&0xff)
    def write_dword(x):
        nonlocal output
        write_word(x&0xffff)
        write_word((x>>16)&0xffff)
    def add_instr(x, a, b, c):
        nonlocal output
        write_word(x)
        write_word(a)
        write_word(b)
        write_word(c)

    p = 0x1000
    error = False
    for i, line in enumerate(s_lines):
        l = line.strip()
        if not l: continue
        if l.startswith(';'): continue
        if REGEX_LABEL.match(l): continue
        splitted = l.split(maxsplit=1)
        t = splitted[0].upper()
        if t == 'REFRESH':
            add_instr(20, 0, 0, 0)
            continue
        if t == 'HLT':
            add_instr(23, 0, 0, 0)
            continue
        if t == 'NOP':
            add_instr(0, 0, 0, 0)
            continue
        if t == 'PUSHRET':
            add_instr(24, 0, 0, 0)
            continue
        if t == 'RET':
            add_instr(25, 0, 0, 0)
            continue
        if t == 'IRET':
            add_instr(27, 0, 0, 0)
            continue
        if t in ['DW', 'DD']:
            matchres = REGEX_REPEAT.match(l)
            if not matchres:
                print(f'ERROR: ({i+1}) wrong format for DW/DD.')
                continue
            times = matchres.groups()[0]
            times = 1 if not times or not times.strip() else int(times)
            pat = [read_imm(x.strip()) for x in matchres.groups()[1].split(',')]
            for _ in range(times):
                for v in pat:
                    if t == 'DW':
                        write_word(v)
                    else:
                        write_dword(v)
            p += times * len(pat)
            continue
        if len(splitted) < 2:
            print(f'ERROR: ({i+1}) rest of the instruction required.', file=sys.stderr)
            error = True
            continue
        rest = splitted[1]
        if t == 'CALL':
            t_res = 1
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 2:
                print(f'ERROR: ({i+1}) 2 arguments required but {len(r)} found.')
                error = True
                continue
            if r[0] not in ['NEAR', 'FAR']:
                print(f'ERROR: ({i+1}) NEAR or FAR required but {r[0]} found')
                error=True
                continue
            if r[0] == 'NEAR':
                target_r = read_reg(r[1]) 
                if (target_r) is None: target_imm = read_imm(r[1])
                if (target_imm) is None: target_label = label_dict.get(r[1])
                if (target_label) is None:
                    print(f'Error: ({i+1}) Rx, imm16 or label required but {r[1]} found.')
                    error = True
                    continue
                add_instr(24, 0, 0, 0)
                p += 4
                if target_r:
                    add_instr(t_res, 0, target_r, 0)
                elif target_imm:
                    add_instr(t_res, 1, target_imm&0xffff, 0)
                elif target_label:
                    if abs(p - target_label['p']) > 0x7fff:
                        print(f'WARN: ({i+1}) location way too far for NEAR. changed to FAR jump automatically.')
                        add_instr(t_res, 3, target_label['p']>>16, target_label['p']&0xffff)
                    else:
                        add_instr(t_res, 1, target_label['p']&0xffff, 0)
            else:
                target_r = read_addr(r[1])
                if (target_r) is None: target_imm = read_imm(r[1])
                if (target_imm) is None: target_label = label_dict.get(r[1])
                if (target_label) is None:
                    print(f'Error: ({i+1}) Rx:Ry or imm32 required but {r[1]} found.')
                    error = True
                    continue
                add_instr(24, 0, 0, 0)
                p += 4
                if target_r:
                    add_instr(t_res, 2, target_r[0], target_r[1])
                elif target_imm:
                    add_instr(t_res, 3, target_imm>>16, target_imm&0xffff)
                elif target_label:
                    add_instr(t_res, 3, target_label['p']>>16, target_label['p']&0xffff)
        elif t in ['JMP', 'JR']:
            t_res = (
                1 if t == 'JMP'
                else 2 if t == 'JR'
                else None  # impossible case.
            )
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 2:
                print(f'ERROR: ({i+1}) 2 arguments required but {len(r)} found.')
                error = True
                continue
            if r[0] not in ['NEAR', 'FAR']:
                print(f'ERROR: ({i+1}) NEAR or FAR required but {r[0]} found')
                error=True
                continue
            if r[0] == 'NEAR':
                target_r = read_reg(r[1]) 
                if (target_r) is None: target_imm = read_imm(r[1])
                if (target_imm) is None: target_label = label_dict.get(r[1])
                if (target_label) is None:
                    print(f'Error: ({i+1}) Rx, imm16 or label required but {r[1]} found.')
                    error = True
                    continue
                if target_r:
                    add_instr(t_res, 0, target_r, 0)
                elif target_imm:
                    add_instr(t_res, 1, target_imm&0xffff, 0)
                elif target_label:
                    if abs(p - target_label['p']) > 0x7fff:
                        print(f'WARN: ({i+1}) location way too far for NEAR. changed to FAR jump automatically.')
                        add_instr(t_res, 3, target_label['p']>>16, target_label['p']&0xffff)
                    else:
                        add_instr(t_res, 1, target_label['p']&0xffff, 0)
            else:
                target_r = read_addr(r[1])
                if (target_r) is None: target_imm = read_imm(r[1])
                if (target_imm) is None: target_label = label_dict.get(r[1])
                if (target_label) is None:
                    print(f'Error: ({i+1}) Rx:Ry or imm32 required but {r[1]} found.')
                    error = True
                    continue
                if target_r:
                    add_instr(t_res, 2, target_r[0], target_r[1])
                elif target_imm:
                    add_instr(t_res, 3, target_imm>>16, target_imm&0xffff)
                elif target_label:
                    add_instr(t_res, 3, target_label['p']>>16, target_label['p']&0xffff)
        elif t in ['JZ', 'JZR']:
            t_res = (3 if t == 'JZ' else 4)
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 3:
                print(f'ERROR: ({i+1}) 3 arguments required but {len(r)} found.')
                error = True
                continue
            if r[0] not in ['NEAR', 'FAR']:
                print(f'ERROR: ({i+1}) NEAR or FAR required but {r[0]} found')
                error=True
                continue
            if r[0] == 'NEAR':
                chk = read_reg(r[1]) 
                if chk is None:
                    print(f'Error: ({i+1}) Register required but {r[1]} found.')
                    error = True
                    continue
                target_r = read_reg(r[2])
                if (target_r) is None: target_imm = read_imm(r[2])
                if (target_imm) is None: target_label = label_dict.get(r[2])
                if (target_label) is None:
                    print(f'Error: ({i+1}) Rx:Ry, imm32 or label required but {r[2]} found.')
                    error = True
                    continue
                if target_r:
                    add_instr(t_res, chk, target_r, 0)
                elif target_imm:
                    add_instr(t_res, 0x0100|chk, target_imm&0xffff, 0)
                elif target_label:
                    if abs(p - target_label['p']) > 0x7fff:
                        print(f'WARN: ({i+1}) location way too far for NEAR. changed to FAR jump automatically.')
                        add_instr(t_res, 0x0300|chk, target_label['p']>>16, target_label['p']&0xffff)
                    else:
                        add_instr(t_res, 0x0100|chk, target_label['p']&0xffff, 0)
            else:
                chk = read_reg(r[1]) 
                if chk is None:
                    print(f'Error: ({i+1}) Register required but {r[1]} found.')
                    error = True
                    continue
                target_r = read_addr(r[2])
                if (target_r) is None: target_imm = read_imm(r[2])
                if (target_imm) is None: target_label = label_dict.get(r[2])
                if (target_label) is None:
                    print(f'Error: ({i+1}) Rx:Ry, imm32 or label required but {r[1]} found.')
                    error = True
                    continue
                if target_r:
                    add_instr(t_res, 0x0200|chk, target_r[0], target_r[1])
                elif target_imm:
                    add_instr(t_res, 0x0300|chk, target_imm>>16, target_imm&0xffff)
                elif target_label:
                    add_instr(t_res, 0x0300|chk, target_label['p']>>16, target_label['p']&0xffff)
        elif t == 'LD':
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 2:
                print(f'ERROR: ({i+1}) 2 arguments required but {len(r)} found.')
                error = True
                continue
            if (a := read_reg(r[0])) is None:
                print(f'Error: ({i+1}) Register required but {r[0]} found.')
                error = True
                continue
            if (b := read_reg(r[1])) is None:
                print(f'Error: ({i+1}) Register required but {r[1]} found.')
                error = True
                continue
            add_instr(5, 0, a, b)
        elif t == 'LDI':
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 2:
                print(f'ERROR: ({i+1}) 2 arguments required but {len(r)} found.')
                error = True
                continue
            if (a := read_reg(r[0])) is None:
                print(f'Error: ({i+1}) Register required but {r[0]} found.')
                error = True
                continue
            if (b := read_imm(r[1])) is None:
                print(f'Error: ({i+1}) Immediate required but {r[1]} found.')
                error = True
                continue
            add_instr(5, 1, a, b)
        elif t == 'LDA':
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 3:
                print(f'ERROR: ({i+1}) 3 arguments required but {len(r)} found.')
                error = True
                continue
            if (a := read_reg(r[0])) is None:
                print(f'Error: ({i+1}) Register required but {r[0]} found.')
                error = True
                continue
            if (b := read_reg(r[1])) is None:
                print(f'Error: ({i+1}) Register required but {r[1]} found.')
                error = True
                continue
            if (lbl := label_dict.get(r[2])) is None:
                print(f'Error: ({i+1}) Cannot find label {r[2]}.')
                error = True
                continue
            add_instr(5, 1, a, lbl['p']&0xffff)
            add_instr(5, 1, b, lbl['p']>>16)
        elif t == 'LDM':
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 2:
                print(f'ERROR: ({i+1}) 2 arguments required but {len(r)} found.')
                error = True
                continue
            if (a := read_reg(r[0])) is None:
                print(f'Error: ({i+1}) Register required but {r[0]} found.')
                error = True
                continue
            if (v := read_addr(r[1])) is None:
                print(f'Error: ({i+1}) Address required but {r[1]} found.')
                error = True
                continue
            add_instr(6, a, v[0], v[1])
        elif t == 'STM':
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 2:
                print(f'ERROR: ({i+1}) 2 arguments required but {len(r)} found.')
                error = TrueR
                continue
            if (v := read_addr(r[0])) is None:
                print(f'Error: ({i+1}) Address required but {r[0]} found.')
                error = True
                continue
            if (a := read_reg(r[1])) is None:
                print(f'Error: ({i+1}) Register required but {r[1]} found.')
                error = True
                continue
            add_instr(7, a, v[0], v[1])
        elif t in ['ADD', 'SUB', 'MUL', 'DIV', 'MOD', 'AND', 'OR', 'XOR', 'LT']:
            t_res = (
                8 if t == 'ADD'
                else 9 if t == 'SUB'
                else 10 if t == 'MUL'
                else 11 if t == 'DIV'
                else 12 if t == 'MOD'
                else 16 if t == 'AND'
                else 17 if t == 'OR'
                else 18 if t == 'XOR'
                else 13 if t == 'LT'
                else None  # impossible case
            )
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 3:
                print(f'ERROR: ({i+1}) 3 arguments required but {len(r)} found.')
                error = True
                continue
            if (a := read_reg(r[0])) is None:
                print(f'Error: ({i+1}) Register required but {r[0]} found.')
                error = True
                continue
            if (b := read_reg(r[1])) is None:
                print(f'Error: ({i+1}) Register required but {r[1]} found.')
                error = True
                continue
            if (c := read_reg(r[2])) is None:
                print(f'Error: ({i+1}) Register required but {r[2]} found.')
                error = True
                continue
            add_instr(t_res, a, b, c)
        elif t == 'OUT':
            r = rest.split(maxsplit=1)
            if (a := read_reg(r[0])) is None:
                print(f'ERROR: ({i+1}) Register required but none found.')
                error = True
                continue
            add_instr(14, a, 0, 0)
        elif t == 'IN':
            r = rest.split(maxsplit=1)
            if (v := read_reg(r[0])) is None:
                print(f'ERROR: ({i+1}) Register required but none found.')
                error = True
                continue
            add_instr(15, v, 0, 0)
        elif t == 'NOT':
            r = [x.strip() for x in rest.split(',')]
            if len(r) < 2:
                print(f'ERROR: ({i+1}) 2 arguments required but {len(r)} found.')
                error = True
                continue
            if (a := read_reg(r[0])) is None:
                print(f'Error: ({i+1}) Register required but {r[0]} found.')
                error = True
                continue
            if (b := read_reg(r[1])) is None:
                print(f'Error: ({i+1}) Register required but {r[1]} found.')
                error = True
                continue
            add_instr(19, a, b, 0)
        elif t == 'PUSH':
            r = rest.split(maxsplit=1)
            if len(r) < 1:
                print(f'ERROR: ({i+1}) 1 arguments required but {len(r)} found.')
                error = True
                continue
            if (v := read_reg(r[0])) is None:
                print(f'ERROR: ({i+1}) Register required but none found.')
                error = True
                continue
            add_instr(21, v, 0, 0)
        elif t == 'POP':
            r = rest.split(maxsplit=1)
            if len(r) < 1:
                print(f'ERROR: ({i+1}) 1 arguments required but {len(r)} found.')
                error = True
                continue
            if (v := read_reg(r[0])) is None:
                print(f'ERROR: ({i+1}) Register required but none found.')
                error = True
                continue
            add_instr(22, v, 0, 0)

        p += 4

    if error: return
    with open(f'{path}.bin', 'wb') as f:
        f.write(bytes(output))

    if not generate_lst_file: return
    with open(f'{path}.lst', 'w') as f:
        z = 0
        p = 0x1000
        cnt = 0
        for i, line in enumerate(s_lines):
            l = line.strip()
            if not l:
                f.write(f'           {l}\n')
            elif l.startswith(';'):
                f.write(f'           {l}\n')
            elif REGEX_LABEL.match(l):
                f.write(f'           {l} ({p:#010x})\n')
            elif REGEX_REPEAT.match(l):
                matchres = REGEX_REPEAT.match(l)
                times = matchres.groups()[0]
                times = 1 if not times or not times.strip() else int(times)
                patlen = len(matchres.groups()[1].split(','))
                for _ in range(times):
                    f.write(f'{p:#010x}    ')
                    for v in range(patlen if l.startswith('DW') else patlen*2):
                        byte1 = output[cnt]
                        byte2 = output[cnt+1]
                        f.write(f'{f"{byte2:#04x}"[2:]}{f"{byte1:#04x}"[2:]} ')
                        cnt += 2
                    f.write('\n')
                    p += patlen if l.startswith('DW') else patlen*2
            else:
                f.write(f'{p:#010x}    ')
                for _ in range(4):
                    byte1 = output[cnt+_*2]
                    byte2 = output[cnt+_*2+1]
                    f.write(f'{f"{byte2:#04x}"[2:]}{f"{byte1:#04x}"[2:]} ')
                f.write('    ')
                f.write(line.strip())
                f.write('\n')
                z += 1
                p += 4
                cnt += 8
                if l.startswith('CALL') or l.startswith('LDA'):
                    f.write(f'{p:#010x}    ')
                    for _ in range(4):
                        byte1 = output[cnt+_*2]
                        byte2 = output[cnt+_*2+1]
                        f.write(f'{f"{byte2:#04x}"[2:]}{f"{byte1:#04x}"[2:]} ')
                    f.write('    ')
                    # f.write(line)
                    f.write('\n')
                    z += 1
                    p += 4
                    cnt += 8


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('usage: srh-chai-asm.py [filename]', file=sys.stderr)
    else:
        main(
            sys.argv[1],
            generate_lst_file=len(sys.argv) > 2 and sys.argv[2] == '-l'
        )
