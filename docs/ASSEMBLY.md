# SRH CHAI assembly language

## Core

```
`JMP`: Jump absolute.
    `JMP NEAR, Rx`: PC ← (PC&0xffff0000)|R[B]
    `JMP NEAR, imm16`: PC ← (PC&0xffff0000)|`B`
    `JMP FAR, Rx:Ry`: PC ← R[B]<<16|R[C]
    `JMP FAR, imm32`: PC ← B:C
`JR`: Jump relative.
    `JR NEAR, Rx`: PC ← PC+R[x]
    `JR NEAR, imm16`: PC ← PC+`B`
    `JR FAR, Rx:Ry`: PC ← PC+(R[B]<<16|R[C])
    `JR FAR, imm32`: PC ← PC+B:C
`JZ`: Jump if Zero Absolute
    `JZ NEAR, Rx, Ry`: PC ← (PC&0xffff0000)|R[B]
    `JZ NEAR, Rx, imm16`: PC ← (PC&0xffff0000)+`B`
    `JZ FAR, Rx, Ry:Rz`: PC ← (R[B]<<16|R)[C]
    `JZ FAR, Rx, imm32`: PC ← imm32(B,C)
`JZR`: Jump if Zero Relative
    `JZR NEAR, Rx, Ry`: PC ← PC+R[B]
    `JZR NEAR, Rx, imm16`: PC ← PC+`B`
    `JZR FAR, Rx, Ry:Rz`: PC ← PC+(R[B]<<16|R)[C]
    `JZR FAR, Rx, imm32`: PC ← PC+imm32(B,C)
`LD Ra, Rb` (when `A` = 0): R[B] ← R[C]
`LDI Ra, imm` (when `A` = 1): R[B] ← C
`LDM Ra, Rb:Rc`: R[A] ← M[Rb:Rc]
`STM Rb:Rc, Ra`: M[Rb:Rc] ← R[a]
`ADD Ra, Rb, Rc`: R[A] ← R[B] + R[C]
`SUB Ra, Rb, Rc`: R[A] ← R[B] - R[C]
`MUL Ra, Rb, Rc`: R[A] ← R[B] × R[C]
`DIV Ra, Rb, Rc`: R[A] ← R[B] ÷ R[C]
`MOD Ra, Rb, Rc`: R[A] ← R[B] modulo R[C]
`LT Ra, Rb, Rc`: R[a] ← 1 if R[b] < R[c]; or else R[a] ← 0
`OUT Rx`: Print the character in R[x]
`IN Rx`: Get one character from the keyboard and store it in R[x]
`AND Ra, Rb, Rc`: R[A] ← R[B] & R[C]
`OR Ra, Rb, Rc`: R[A] ← R[B] | R[C]
`XOR Ra, Rb, Rc`: R[A] ← R[B] ^ R[C]
`NOT Ra, Rb`: R[A] ← ~R[B]
`REFRESH`: Refresh the screen
`PUSH Rx`: M[SS:SP] ← R[x]; SP -= 1
`POP Rx`: SP += 1; R[x] ← M[SS:SP]
`HLT`: Halt the machine.
`PUSHRET`: M[SS:SP] ← (PC+8)>>16; M[SS:SP-1] = (PC+8)&0xffff; SP -= 2;
      + Use with `JMP`/`JR`/`JZ`/`JZR`.
`RET`: PC ← (M[SS:SP-2]<<16)|M[SS:SP-1]; SP += 2
`IRET`: PC ← PC_BACKUP
```

## Additional instructions

+ `NOP`: does nothing.
+ `LDA Ra, Rb, label`: R[a] = label&amp;0xffff; R[b] = label>>16;
+ `CALL`: Call. (a combination of `PUSHRET` and `JMP`.)

## Pseudo-instructions

+ `DW const1, const2, const3, ...`
+ `DD const1, const2, const3, ...`
+ `DW REPEAT(times) const1, const2, const3, ...`:
+ `DD REPEAT(times) const1, const2, const3, ...`


## How to write time interrupt handler

```
TIMER_INTERRUPT:
    PUSH XH
    PUSH XL
    PUSH R0
    LDI XH, 0
    LDI XL, 0x201
    LDI R0, 0
    STM XH:XL, R0
    POP R0
    POP XL
    POP XH

    ; ...
    
    PUSH XH
    PUSH XL
    PUSH R0
    LDI XH, 0
    LDI XL, 0x200
    ; must be a number bigger than 4.
    LDI R0, 16
    STM XH:XL, R0
    LDI XL, 0x201
    LDI R0, 1
    STM XH:XL, R0
    POP R0
    POP XL
    POP XH
    IRET

MAIN:
    LDA R0, R1, TIMER_INTERRUPT
    LDI XH, 0
    LDI XL, 0x100
    STM XH:XL, R0
    LDI XL, 0x101
    STM XH:XL, R1
```

`example/hello5.asm` is an example of this; this is how you do preemptive multitasking in CHAI.

## How do I save regs

The current convention is:

+ Callee save R0~R7, XH, XL, YH, YL
+ Caller save R8~R15

I use R8~R15 for temporary constants (because algorithmic instructions only support operations between registers as of now) so in a sense all registers are callee-saved for me.
