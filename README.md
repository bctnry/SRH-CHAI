# SRH CHAI

CHifir-inspired Application Infrastructure

(very early draft; could change drastically.)

## What's available now:

+ An emulator written in c (`main_cli.c`) that supports time interrupt and can do text input/output.
+ An single-file assembler written in python (`asm.py`) that supports a fair bit of functionalities. (read `docs/ASSEMBLY.md` for more info & `examples` for examples.)

## What's down the line

+ BASIC interpreter.
+ PL/0 (and maybe PL/M as well) compiler.
+ Add support for "block device"
+ Oberon (maybe).
+ Forth interpreter.
+ (... and other stuff I couldn't recall right now.)

## VM

+ Word size 16 bits.
+ A byte-addressed memory ranging from 512KBytes/262140 words to 32MBytes/16777215 words
  + i.e. 0x0~0x1ffff to 0x0~0xffffff
+ Program load at 0x1000. 0x0~0xfff reserved.
  <!-- + 0x01, 0x02: screen width & height. -->
  <!-- + 0x03, 0x04, 0x05: mouse x, y and buttons. -->
  <!-- + 0x06: keyboard modifier -->
  + 0x10, 0x11: available memory size in words subtracted by 0x1000 (little-endian)
  + 0x20: time interrupt counter. decreases by 1 for each instruction executed.
    + 0x21: time counter enabling register. non-zero means enabled.
  + 0x100~0x1ff: interrupt vector table (128 interrupts) (little-endian)
    + 0x100, 0x101: time interrupt.
+ Sometimes a "BIOS" would load at 0x1000~0xffff.
+ A program counter PC with a length of 32 bits (higher bit not used)
+ A register file of 16 registers
+ Four separate registers for addressing: XH, XL, YH, YL (also named as R16~R19)
+ Two separate registers for stack: SS and SP (also named as R20 and R21).
  + default to SS=0x0 SP=0xffff at startup.
+ Instead of the overlapping way like in 8086 XH, YH and SS actually directly corresponds to the higher bit of the address.
+ A memory mapped display of maximum 1024x768 32K-color (5-bit RGB) bitmap display with one word per pixel
  + (memory at 0x40000~0x100000)
  + screen could change size by writing M[0x01] and M[0x02], and one does not need 2MBytes/1048576 words of RAM if their requirement for screen is small; e.g. if one only wants a 320x200 screen the memory could be 0x40000~0x4fa00.
+ Each instruction is made up of 4 words: one operator and three operands, named as `A`, `B` and `C` respectively.
  + The instruction set is defined as follows:
    1. `JMP`: Jump absolute.
      1. `JMP NEAR, Rx` (when `A` = 0): PC ← (PC&0xffff0000)|R[B]
      2. `JMP NEAR, imm16` (when `A` = 1): PC ← (PC&0xffff0000)|`B`
      3. `JMP FAR, Rx:Ry` (when `A` = 2): PC ← R[B]<<16|R[C]
      4. `JMP FAR, imm32` (when `A` = 3): PC ← B:C
    2. `JR`: Jump relative.
      1. `JR NEAR, Rx` (when `A` = 0): PC ← PC+R[x]
      2. `JR NEAR, imm16` (when `A` = 1): PC ← PC+`B`
      3. `JR FAR, Rx:Ry` (when `A` = 2): PC ← PC+(R[B]<<16|R[C])
      4. `JR FAR, imm32` (when `A` = 3): PC ← PC+B:C
    3. `JZ`: Jump if Zero Absolute
      1. `JZ NEAR, Rx, Ry` (when bit9&bit8 of `A` = 0): PC ← (PC&0xffff0000)|R[B]
      2. `JZ NEAR, Rx, imm16` (when bit9&bit8 of `A` = 1): PC ← (PC&0xffff0000)+`B`
      3. `JZ FAR, Rx, Ry:Rz` (when bit9&bit8 of `A` = 2): PC ← (R[B]<<16|R)[C]
      4. `JZ FAR, Rx, imm32` (when bit9&bit8 of `A` = 3): PC ← imm32(B,C)
    4. `JZR`: Jump if Zero rELATIVE
      1. `JZR NEAR, Rx, Ry` (when bit9&bit8 of `A` = 0): PC ← PC+R[B]
      2. `JZR NEAR, Rx, imm16` (when bit9&bit8 of `A` = 1): PC ← PC+`B`
      3. `JZR FAR, Rx, Ry:Rz` (when bit9&bit8 of `A` = 2): PC ← PC+(R[B]<<16|R)[C]
      4. `JZR FAR, Rx, imm32` (when bit9&bit8 of `A` = 3): PC ← PC+imm32(B,C)
      
    5. `LD Ra, Rb` (when `A` = 0): R[B] ← R[C]
      1. `LDI Ra, imm` (when `A` = 1): R[B] ← C
    6. `LDM Ra, Rb:Rc`: R[A] ← M[Rb:Rc]
    7. `STM Rb:Rc, Ra`: M[Rb:Rc] ← R[a]
    8. `ADD Ra, Rb, Rc`: R[A] ← R[B] + R[C]
    9. `SUB Ra, Rb, Rc`: R[A] ← R[B] - R[C]
    10. `MUL Ra, Rb, Rc`: R[A] ← R[B] × R[C]
    11. `DIV Ra, Rb, Rc`: R[A] ← R[B] ÷ R[C]
    12. `MOD Ra, Rb, Rc`: R[A] ← R[B] modulo R[C]
    13. `LT Ra, Rb, Rc`: R[a] ← 1 if R[b] < R[c]; or else R[a] ← 0
    14. `OUT Rx`: Print the character in R[x]
    15. `IN Rx`: Get one character from the keyboard and store it in R[x]
    16. `AND Ra, Rb, Rc`: R[A] ← R[B] & R[C]
    17. `OR Ra, Rb, Rc`: R[A] ← R[B] | R[C]
    18. `XOR Ra, Rb, Rc`: R[A] ← R[B] ^ R[C]
    19. `NOT Ra, Rb`: R[A] ← ~R[B]
    20. `REFRESH`: Refresh the screen
    21. `PUSH Rx`: M[SS:SP] ← R[x]; SP -= 1
    22. `POP Rx`: SP += 1; R[x] ← M[SS:SP]
    23. `HLT`: Halt the machine.
    24. `PUSHRET`: M[SS:SP] ← (PC+8)>>16; M[SS:SP-1] = (PC+8)&0xffff; SP -= 2;
      + Use with `JMP`/`JR`/`JZ`/`JZR`.
    25. `RET`: PC ← (M[SS:SP-2]<<16)|M[SS:SP-1]; SP += 2
    26. (skip)
    27. `IRET`: PC ← PC_BACKUP
      + When an interrupt happens PC is stored in PC_BACKUP instead of using the stack with stacktop located at SS:SP.

